﻿-- Database : `mc_db`
CREATE DATABASE IF NOT EXISTS `mc_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `mc_db`;

-- テーブル : `t_user`
CREATE TABLE `t_user`(
 `id` SERIAL,
 `name` varchar(101) NOT NULL,
 `login_id` varchar(101) UNIQUE NOT NULL,
 `login_password` varchar(101) NOT NULL,
 `introduction` text(301)
 );
 
-- テーブル : `t_rental`
CREATE TABLE `t_rental`(
 `id` SERIAL,
 `user_id` int(11) NOT NULL,
 `start_date` datetime NOT NULL,
 `end_date` datetime NOT NULL,
 `message` text(301),
 `request_date` datetime NOT NULL,
 `item_id` int(11) NOT NULL
 );

--テーブル削除
--USE `mc_db`;
--DELETE FROM ``;
--DROP TABLE ``;

-- テーブル : `t_item`
CREATE TABLE `t_item`(
 `id` SERIAL,
 `user_id` int(11) NOT NULL,
 `instrument` varchar(101) NOT NULL,
 `Manufacturer` varchar(101) NOT NULL,
 `start_date` datetime NOT NULL,
 `end_date` datetime NOT NULL,
 `price` int(11) NOT NULL,
 `file_name` varchar(101) NOT NULL,
 `rentalFlag` int (11) NOT NULL
 );
-- CREATEしたTABLEにカラムを追加
 alter table t_item add `itemCategory_id` int(11) NOT NULL;
 
-- テーブル : `t_itemCategory`
CREATE TABLE `t_itemCategory`(
 `id` SERIAL,
 `Category_name` varchar(101) NOT NULL
 );
-- INSERT : `t_itemCategory`
 INSERT INTO `t_itemCategory` (`id`, `Category_name`) VALUES
(1, '弦楽器'),
(2, '管楽器'),
(3, '鍵盤楽器'),
(4, '打楽器'),
(5, 'その他');
 

-- テーブル : `t_itemCategoryData`
CREATE TABLE `t_itemCategoryData`(
 `id` SERIAL,
 `Category_id` int(11) NOT NULL,
 `CategoryData_name` varchar(101) NOT NULL
 
 );
-- INSERT : `t_itemCategoryData`
 INSERT INTO `t_itemCategoryData` (`Category_id`, `CategoryData_name`) VALUES
(1, 'バイオリン'),
(1, 'ビオラ'),
(1, 'チェロ'),
(1, 'コントラバス'),
(1, 'ギター'),
(1, 'その他【弦楽器】'),
(2, 'フルート'),
(2, 'オーボエ'),
(2, 'クラリネット'),
(2, 'サクソフォン'),
(2, 'トランペット'),
(2, 'トロンボーン'),
(2, 'ホルン'),
(2, 'ユーフォニアム'),
(2, 'チューバ'),
(2, 'その他【管楽器】'),
(3, 'ピアノ'),
(3, 'エレクトーン'),
(3, 'その他【鍵盤楽器】'),
(4, '打楽器'),
(5, 'その他');
