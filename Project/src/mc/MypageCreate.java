package mc;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * User新規登録
 */
@WebServlet("/MypageCreate")
public class MypageCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/MypageCreate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

		String loginId = request.getParameter("login_id");
		String UserName = request.getParameter("name");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String introduction = request.getParameter("introduction");

		UserDataBeans udb = new UserDataBeans();

		udb.setLoginId(loginId);
		udb.setName(UserName);
		udb.setPassword(password);
		udb.setIntroduction(introduction);

		if(!password.equals(password2) || UserDAO.findByloginId(udb.getLoginId(), 0)) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			request.getRequestDispatcher("/WEB-INF/jsp/MypageCreate.jsp").forward(request, response);
			return;
		}

		UserDAO.insertUser(udb);
		session.setAttribute("udb", udb);

		request.getRequestDispatcher("/WEB-INF/jsp/Mypage.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
