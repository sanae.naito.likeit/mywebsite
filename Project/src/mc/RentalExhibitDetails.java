package mc;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;
import dao.RentalDAO;

/**
 * Servlet implementation class RentalExhibitDetails
 */
@WebServlet("/RentalExhibitDetails")
public class RentalExhibitDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
		int itemId = Integer.parseInt(request.getParameter("id"));

		   ItemDataBeans idb = ItemDAO.ItemIdDetails(itemId);
		   request.setAttribute("idb",idb);

		   request.getRequestDispatcher("/WEB-INF/jsp/RentalExhibitDetails.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Myitem更新
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			int ItemId = Integer.parseInt(request.getParameter("id"));
			String Instrument = request.getParameter("instrument");
			String Manufacturer = request.getParameter("Manufacturer");
			Date startDay = java.sql.Date.valueOf(request.getParameter("start_date"));
			Date endDay = java.sql.Date.valueOf(request.getParameter("end_date"));
			int RA = Integer.parseInt(request.getParameter("price"));

			ItemDataBeans idb = new ItemDataBeans();

			idb.setId(ItemId);
			idb.setInstrument(Instrument);
			idb.setManufacturer(Manufacturer);
			idb.setStartDate(startDay);
			idb.setEndDate(endDay);
			idb.setPrice(RA);

			ItemDataBeans idb2 = ItemDAO.ItemIdDetails(ItemId);

			//日付の条件
			int date = ItemDAO.nowdifferenceDays(startDay);
			int date2 = ItemDAO.nowdifferenceDays(endDay);
			int DATE = RentalDAO.differenceDays(startDay, endDay);

			if(0 > date || 0 > date2 || 0 > DATE) {

				request.setAttribute("errMsg", "入力された内容は正しくありません");

				request.setAttribute("idb",idb2);

				request.getRequestDispatcher("/WEB-INF/jsp/RentalExhibitDetails.jsp").forward(request, response);
				return;
			}


		ItemDAO.Update(idb);

		response.sendRedirect("Mypage");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
