package mc;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemCategoryDataBeans;
import dao.ItemCategoryDataDAO;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//セッションに入ってる「search」を消去する
		session.removeAttribute("search");

		try {
		ArrayList<ItemCategoryDataBeans> CategoryList;
		CategoryList = (ArrayList<ItemCategoryDataBeans>)ItemCategoryDataDAO.findAll();
		request.setAttribute("CategoryList", CategoryList);
		//jspにフォワード
		request.getRequestDispatcher("/WEB-INF/jsp/Home.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
