package mc;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.RentalDataBeans;
import dao.ItemDAO;
import dao.RentalDAO;

/**
 * Servlet implementation class RentalDetails
 */
@WebServlet("/RentalDetails")
public class RentalDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
		int Rentalid = Integer.parseInt(request.getParameter("id"));

		//レンタル情報
		RentalDataBeans rdb = RentalDAO.RentalidDetails(Rentalid);
		request.setAttribute("RentalInfo", rdb);

		//レンタル商品
		int ItemId = rdb.getItemId();
		ItemDataBeans idb = ItemDAO.ItemIdDetails(ItemId);
		request.setAttribute("item",idb);

		//[レンタル合計金額]
				//①レンタル期間
				Date startDate = rdb.getStartDate();
				Date endDate = rdb.getEndDate();
				int rentalDays = RentalDAO.differenceDays(startDate, endDate);
				//②合計金額
				int itemPrice = idb.getPrice();
				int totalPrice = RentalDAO.getTotalPrice(rentalDays, itemPrice);
				String formatTotalPrice = rdb.getFormatTotalPrice(totalPrice);
				request.setAttribute("formatTotalPrice",formatTotalPrice);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/RentalDetails.jsp");
		dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
