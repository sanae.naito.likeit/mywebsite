package mc;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.RentalDataBeans;
import dao.ItemDAO;
import dao.RentalDAO;

/**
 * Servlet implementation class Rental
 */
@WebServlet("/Rental")
public class Rental extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			//ログインしてない場合は、ログイン画面へ
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;
			if (!isLogin) {
				// Sessionにリターンページ情報を書き込む
				session.setAttribute("returnRental", "Rental");
				// Login画面にリダイレクト
				response.sendRedirect("Login");

			} else {
				//もし、セッションにレンタル情報が入ってたら、消去する
				RentalDataBeans rdb = (RentalDataBeans) session.getAttribute("rdb");
				if(rdb != null) {
					session.removeAttribute("rdb");
				}

			int ItemId = Integer.parseInt(request.getParameter("id"));

			ItemDataBeans idb = ItemDAO.ItemIdDetails(ItemId);
			   request.setAttribute("item",idb);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Rental.jsp");
			dispatcher.forward(request, response);

			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				request.setCharacterEncoding("UTF-8");

				HttpSession session = request.getSession();

				try {

				int UserId = (int)session.getAttribute("userInfo");
				int ItemId = Integer.parseInt(request.getParameter("item_id"));
				String Messege = request.getParameter("Messege");
				Date startDay = java.sql.Date.valueOf(request.getParameter("start_date"));
				Date endDay = java.sql.Date.valueOf(request.getParameter("end_date"));

				RentalDataBeans rdb = new RentalDataBeans();
				rdb.setUserId(UserId);
				rdb.setItemId(ItemId);
				rdb.setMessage(Messege);
				rdb.setStartDate(startDay);
				rdb.setEndDate(endDay);

				//日付条件
				ItemDataBeans idb = ItemDAO.ItemIdDetails(ItemId);

				int ItemUserId = idb.getUserId();

				Date itemStart = idb.getStartDate();
				Date itemEnd = idb.getEndDate();

				int date1 = RentalDAO.differenceDays(itemStart, startDay);
				int date2 = RentalDAO.differenceDays(endDay, itemEnd);
				int DATE = RentalDAO.differenceDays(startDay, endDay);

				if(0 > date1 || 0 > date2 || 0 > DATE || UserId == ItemUserId) {

				request.setAttribute("errMsg", "入力された内容は正しくありません");

				request.setAttribute("item", idb);

				request.getRequestDispatcher("/WEB-INF/jsp/Rental.jsp").forward(request, response);
				return;
				}

				session.setAttribute("rdb", rdb);
				response.sendRedirect("RentalConfirmatiom");

				} catch (Exception e) {
					e.printStackTrace();
					session.setAttribute("errorMessage", e.toString());
					response.sendRedirect("Error");
				}
			}
		}