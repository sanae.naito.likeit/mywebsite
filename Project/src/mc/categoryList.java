package mc;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class categoryList
 */
@WebServlet("/categoryList")
public class categoryList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static int PAGEMAX = 4;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//セッションに入ってる「search」を消去する
			session.removeAttribute("search");

			int category = Integer.parseInt(request.getParameter("id"));

			//カテゴリー検索
				int PageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
				session.setAttribute("search", category);
				ArrayList<ItemDataBeans> searchItemList = ItemDAO.searchCategory(category, PageNum, PAGEMAX);
				double ItemCount = ItemDAO.CategoryCount(category);
				int pagemax = (int)Math.ceil(ItemCount / PAGEMAX);

				//全アイテム数
				request.setAttribute("ItemCount", (int)ItemCount);
				//全ページ数
				request.setAttribute("pageMax", pagemax);
				//表示ページ
				request.setAttribute("pageNum", PageNum);
				request.setAttribute("itemList", searchItemList);

				request.getRequestDispatcher("/WEB-INF/jsp/itemList.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
