package mc;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.RentalDataBeans;
import dao.ItemDAO;
import dao.RentalDAO;

/**
 * Servlet implementation class RentalResult
 */
@WebServlet("/RentalResult")
public class RentalResult extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
		//セッションのレンタル情報を削除
		session.removeAttribute("rdb");

		//セッションからRentalidを取得
		int Rental_id = (int)session.getAttribute("Rentalid");

		RentalDataBeans RentalInfo = RentalDAO.RentalidDetails(Rental_id);
		request.setAttribute("RentalInfo", RentalInfo);

		//セッションからRentalidを削除
		session.removeAttribute("Rentalid");

		int ItemId = RentalInfo.getItemId();
		ItemDataBeans idb = ItemDAO.ItemIdDetails(ItemId);
		request.setAttribute("item", idb);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/RentalResult.jsp");
		dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
