package mc;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.RentalDataBeans;
import beans.UserDataBeans;
import dao.ItemDAO;
import dao.RentalDAO;
import dao.UserDAO;

/**
 * Servlet implementation class Mypage
 */
@WebServlet("/Mypage")
public class Mypage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			//ログインしてない場合は、ログイン画面へ
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;
			if (!isLogin) {
				// Sessionにリターンページ情報を書き込む
				session.setAttribute("returnRental", "Rental");
				// Login画面にリダイレクト
				response.sendRedirect("Login");

			} else {

		int userId = (int)session.getAttribute("userInfo");

		//User
			UserDataBeans udb = UserDAO.Details(userId);
			request.setAttribute("udb",udb);

		//Rental
		   ArrayList<RentalDataBeans> RentalList = RentalDAO.UserIdDetails(userId);
		   request.setAttribute("RentalList", RentalList);

		//Exhibit
		   ArrayList<ItemDataBeans> itemList = ItemDAO.Details(userId);
		   session.setAttribute("itemList",itemList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mypage.jsp");
		dispatcher.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// MyUser更新
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

		int UserId = Integer.parseInt(request.getParameter("id"));
		String UserName = request.getParameter("name");
		String introduction = request.getParameter("introduction");

		UserDataBeans udb = new UserDataBeans();

		udb.setId(UserId);
		udb.setName(UserName);
		udb.setIntroduction(introduction);

		UserDAO.Update(udb);

		response.sendRedirect("Mypage");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
