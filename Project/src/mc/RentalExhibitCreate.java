package mc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemCategoryDataBeans;
import beans.ItemDataBeans;
import dao.ItemCategoryDataDAO;
import dao.ItemDAO;
import dao.RentalDAO;

/**
 * Servlet implementation class RentalExhibitCreate
 */
@WebServlet("/RentalExhibitCreate")
public class RentalExhibitCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		try {

		//ログインしてない場合は、ログイン画面へ
		Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;
		if (!isLogin) {
			// Sessionにリターンページ情報を書き込む
			session.setAttribute("returnRental", "Rental");
			// Login画面にリダイレクト
			response.sendRedirect("Login");

		} else {

		ArrayList<ItemCategoryDataBeans> CategoryList = (ArrayList<ItemCategoryDataBeans>)ItemCategoryDataDAO.findAll();
		request.setAttribute("CategoryList", CategoryList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/RentalExhibitCreate.jsp");
		dispatcher.forward(request, response);

		}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				request.setCharacterEncoding("UTF-8");

				HttpSession session = request.getSession();

				try {

				int UserId = (int)session.getAttribute("userInfo");
				String File = request.getParameter("file_name");
				String Instrument = request.getParameter("instrument");
				String Manufacturer = request.getParameter("Manufacturer");
				Date startDay = java.sql.Date.valueOf(request.getParameter("start_date"));
				Date endDay = java.sql.Date.valueOf(request.getParameter("end_date"));
				int RA = Integer.parseInt(request.getParameter("price"));
				int itemCategoryId = Integer.parseInt(request.getParameter("id"));

				ItemDataBeans idb = new ItemDataBeans();
				idb.setUserId(UserId);
				idb.setFileName(File);
				idb.setInstrument(Instrument);
				idb.setManufacturer(Manufacturer);
				idb.setStartDate(startDay);
				idb.setEndDate(endDay);
				idb.setPrice(RA);
				idb.setItemCategoryId(itemCategoryId);

				//日付の条件
				int date = ItemDAO.nowdifferenceDays(startDay);
				int date2 = ItemDAO.nowdifferenceDays(endDay);
				int DATE = RentalDAO.differenceDays(startDay, endDay);

				if(0 > date || 0 > date2 || 0 > DATE) {

					request.setAttribute("errMsg", "入力された内容は正しくありません");

					request.getRequestDispatcher("/WEB-INF/jsp/RentalExhibitCreate.jsp").forward(request, response);
					return;
				}

				ItemDAO.insertItem(idb);

				response.sendRedirect("Mypage");

				} catch (Exception e) {
					e.printStackTrace();
					session.setAttribute("errorMessage", e.toString());
					response.sendRedirect("Error");
				}
			}
		}