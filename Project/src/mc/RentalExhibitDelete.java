package mc;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class RentalExhibitDelete
 */
@WebServlet("/RentalExhibitDelete")
public class RentalExhibitDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			int itemId = Integer.parseInt(request.getParameter("id"));

			   ItemDataBeans idb = ItemDAO.ItemIdDetails(itemId);
			   request.setAttribute("idb",idb);

			   request.getRequestDispatcher("/WEB-INF/jsp/RentalExhibitDelete.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			request.setCharacterEncoding("UTF-8");
			HttpSession session = request.getSession();
			try {

				int itemId = Integer.parseInt(request.getParameter("id"));
				ItemDAO.Delete(itemId);
				response.sendRedirect("Mypage");

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
		}
	}