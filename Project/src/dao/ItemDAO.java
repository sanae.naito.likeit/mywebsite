package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDAO {

	//[条件]日付
	public static int nowdifferenceDays(Date inputDate) {
		//現在日付を取得
		Date nowDate = new Date();
	    long nowDateTime = nowDate.getTime();
	    long inputDateTime = inputDate.getTime();
	    long one_date_time = 1000 * 60 * 60 * 24;
	    long diffDays = (inputDateTime - nowDateTime) / one_date_time;
	    return (int)diffDays;
	}

	//★レンタル時 t_item.rentalFlagを1にする
		public static ItemDataBeans upDateRentalFlag1(int Itemid) throws SQLException {
			ItemDataBeans idb = new ItemDataBeans();
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement(
						"UPDATE t_item SET rentalFlag = 1 WHERE id = ?");
				st.setInt(1,Itemid);
				st.executeUpdate();

				System.out.println("update t_item rentalFlag1 has been completed");
				return idb;

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}

		//★item戻り時 t_item.rentalFlagを2にする
		public static ItemDataBeans upDateRentalFlag2(int Itemid) throws SQLException {
			ItemDataBeans idb = new ItemDataBeans();
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement(
						"UPDATE t_item SET rentalFlag = 2 WHERE id = ?");
				st.setInt(1,Itemid);
				st.executeUpdate();

				System.out.println("update t_item rentalFlag2 has been completed");
				return idb;

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
	//新規登録
		public static void insertItem(ItemDataBeans idb) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("INSERT INTO t_item(user_id,instrument,Manufacturer,start_date,end_date,price,file_name,rentalFlag,itemCategory_id) VALUES(?,?,?,?,?,?,?,0,?)");
				st.setInt(1,idb.getUserId());
				st.setString(2,idb.getInstrument());
				st.setString(3,idb.getManufacturer());
				st.setString(4,idb.getFormatDate());
				st.setString(5, idb.getFormatDate2());
				st.setInt(6, idb.getPrice());
				st.setString(7, idb.getFileName());
				st.setInt(8,idb.getItemCategoryId());
				st.executeUpdate();
				System.out.println("inserting item has been completed");

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
	//Mypage[item詳細]
		public static ArrayList<ItemDataBeans> Details(int userId) throws SQLException{
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT * FROM t_item WHERE user_id = ?");
				st.setInt(1, userId);

				ResultSet rs = st.executeQuery();

				ArrayList<ItemDataBeans> ItemList = new ArrayList<ItemDataBeans>();

				while (rs.next()) {
					ItemDataBeans idb = new ItemDataBeans();
					idb.setId(rs.getInt("id"));
					idb.setUserId(rs.getInt("user_id"));
					idb.setInstrument(rs.getString("instrument"));
					idb.setManufacturer(rs.getString("Manufacturer"));
					idb.setStartDate(rs.getDate("start_date"));
					idb.setEndDate(rs.getDate("end_date"));
					idb.setPrice(rs.getInt("price"));
					idb.setFileName(rs.getString("file_name"));
					idb.setRentalFlag(rs.getInt("rentalFlag"));
					ItemList.add(idb);
				}

				System.out.println("searching ItemDataBeans by userId has been completed");
				return ItemList;

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
		//item詳細[itemIdによる]
				public static ItemDataBeans ItemIdDetails(int ItemId) throws SQLException{
					Connection con = null;
					PreparedStatement st = null;
					try {
						con = DBManager.getConnection();

						st = con.prepareStatement("SELECT * FROM t_item "
								+ "JOIN t_user "
								+ "ON t_item.user_id = t_user.id "
								+ "WHERE t_item.id = ?");

						st.setInt(1, ItemId);

						ResultSet rs = st.executeQuery();

						ItemDataBeans idb = new ItemDataBeans();

						if(rs.next()) {
							idb.setId(rs.getInt("id"));
							idb.setUserId(rs.getInt("user_id"));
							idb.setInstrument(rs.getString("instrument"));
							idb.setManufacturer(rs.getString("Manufacturer"));
							idb.setStartDate(rs.getDate("start_date"));
							idb.setEndDate(rs.getDate("end_date"));
							idb.setPrice(rs.getInt("price"));
							idb.setFileName(rs.getString("file_name"));
							idb.setUserName(rs.getString("name"));
							idb.setRentalFlag(rs.getInt("rentalFlag"));
						}

						System.out.println("searching item by itemID has been completed");
						return idb;

					} catch (SQLException e) {
						System.out.println(e.getMessage());
						throw new SQLException(e);
					} finally {
						if (con != null) {
							con.close();
						}
					}
				}
	//item更新
		public static void Update(ItemDataBeans idb) throws SQLException{
			//ItemDataBeans UPidb = new ItemDataBeans();
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("UPDATE t_item SET instrument = ?,Manufacturer = ?,start_date = ?,end_date = ?,price = ? WHERE id = ?");
				st.setString(1,idb.getInstrument());
				st.setString(2,idb.getManufacturer());
				st.setString(3,idb.getFormatDate());
				st.setString(4, idb.getFormatDate2());
				st.setInt(5, idb.getPrice());
				st.setInt(6, idb.getId());

				st.executeUpdate();
				System.out.println("update has been completed");

				} catch (SQLException e) {
					System.out.println(e.getMessage());
					throw new SQLException(e);
				} finally {
					if (con != null) {
						con.close();
					}
				}
			}

	//item削除
		public static void Delete(int ItemId) throws SQLException{
			Connection con = null;
			PreparedStatement st = null;

			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("DELETE FROM t_item WHERE id = ?");
				st.setInt(1, ItemId);
				st.executeUpdate();
				System.out.println("delete item has been completed");
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
	//商品検索
		public static ArrayList<ItemDataBeans> searchItem(String search,int pageNum,int pageMIC) throws SQLException{
			Connection con = null;
			PreparedStatement st = null;
			try {
				int startNum = (pageNum - 1) * pageMIC ;
				con = DBManager.getConnection();

				if(search.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM t_item "
						+ "JOIN t_user "
						+ "ON t_item.user_id = t_user.id "
						+ "WHERE t_item.end_date > now() "
						+ "AND t_item.rentalFlag = 0 "
						+ "ORDER BY t_item.id ASC LIMIT ?,?");
				st.setInt(1,startNum);
				st.setInt(2,pageMIC);
				} else {
				// 名前検索
				st = con.prepareStatement("SELECT * FROM t_item "
						+ "JOIN t_user "
						+ "ON t_item.user_id = t_user.id "
						+ "WHERE t_item.instrument LIKE ? "
						+ "AND t_item.end_date > now() "
						+ "AND t_item.rentalFlag = 0 "
						+ "ORDER BY t_item.id ASC LIMIT ?,?");

				st.setString(1, "%" + search + "%");
				st.setInt(2,startNum);
				st.setInt(3,pageMIC);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while(rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setUserId(rs.getInt("user_id"));
				idb.setInstrument(rs.getString("instrument"));
				idb.setManufacturer(rs.getString("Manufacturer"));
				idb.setFileName(rs.getString("file_name"));
				idb.setStartDate(rs.getDate("start_date"));
				idb.setEndDate(rs.getDate("end_date"));
				idb.setPrice(rs.getInt("price"));
				idb.setUserName(rs.getString("name"));
				idb.setRentalFlag(rs.getInt("rentalFlag"));
				itemList.add(idb);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//商品検索[総数]
		public static double ItemCount(String search) throws SQLException{
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("SELECT COUNT(*) AS CNT FROM t_item"
												+ " WHERE instrument LIKE ?"
												+ " AND end_date > now()"
												+ " AND rentalFlag = 0");
				st.setString(1, "%" + search + "%");
				ResultSet rs = st.executeQuery();
				double cou = 0.0;
				while(rs.next()) {
					cou = Double.parseDouble(rs.getString("cnt"));
				}
				return cou;

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
				con.close();
				}
			}
		}

	//カテゴリー検索
		public static ArrayList<ItemDataBeans> searchCategory(int category,int pageNum,int pageMIC) throws SQLException{
			Connection con = null;
			PreparedStatement st = null;
			try {
				int startNum = (pageNum - 1) * pageMIC ;
				con = DBManager.getConnection();
				st = con.prepareStatement("SELECT * FROM t_item "
						+ "JOIN t_user "
						+ "ON t_item.user_id = t_user.id "
						+ "WHERE t_item.itemCategory_id = ? "
						+ "AND t_item.end_date > now() "
						+ "AND t_item.rentalFlag = 0 "
						+ "ORDER BY t_item.id ASC LIMIT ?,?");

				st.setInt(1,category);
				st.setInt(2,startNum);
				st.setInt(3,pageMIC);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while(rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setUserId(rs.getInt("user_id"));
				idb.setInstrument(rs.getString("instrument"));
				idb.setManufacturer(rs.getString("Manufacturer"));
				idb.setFileName(rs.getString("file_name"));
				idb.setStartDate(rs.getDate("start_date"));
				idb.setEndDate(rs.getDate("end_date"));
				idb.setPrice(rs.getInt("price"));
				idb.setUserName(rs.getString("name"));
				idb.setRentalFlag(rs.getInt("rentalFlag"));
				itemList.add(idb);
			}
			System.out.println("get Items by categoryId has been completed");
			return itemList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//カテゴリー検索[総数]
		public static double CategoryCount(int category) throws SQLException{
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("SELECT COUNT(*) AS CNT FROM t_item"
												+ " WHERE itemCategory_id = ?"
												+ " AND end_date > now()"
												+ " AND rentalFlag = 0");
				st.setInt(1, category);
				ResultSet rs = st.executeQuery();
				double cou = 0.0;
				while(rs.next()) {
					cou = Double.parseDouble(rs.getString("cnt"));
				}
				return cou;

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}

		}
}
