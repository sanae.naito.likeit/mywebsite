package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import base.DBManager;
import beans.RentalDataBeans;

public class RentalDAO {

	//①期間を算出
	public static int differenceDays(Date Date1,Date Date2) {
	    long DateTime1 = Date1.getTime();
	    long DateTime2 = Date2.getTime();
	    long one_date_time = 1000 * 60 * 60 * 24;
	    long diffDays = (DateTime2 - DateTime1) / one_date_time;
	    return (int)diffDays;
	}
	//②レンタル料金の合計金額
	public static int getTotalPrice(int rentalDays,int itemPrice) {
		int total = rentalDays * itemPrice ;
		return total;
	}

	//レンタル情報登録
	public static int insertRenatal(RentalDataBeans rdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_rental(user_id,start_date,end_date,message,item_id,request_date) VALUES(?,?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1,rdb.getUserId());
			st.setString(2,rdb.getFormatDate());
			st.setString(3,rdb.getFormatDate2());
			st.setString(4,rdb.getMessage());
			st.setInt(5,rdb.getItemId());
			st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if(rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting rental has been completed");
			return autoIncKey;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//★レンタル情報取得[Rentalid]
	public static RentalDataBeans RentalidDetails(int Id) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_rental WHERE id = ?");

			st.setInt(1, Id);

			ResultSet rs = st.executeQuery();

			RentalDataBeans rdb = new RentalDataBeans();

			if(rs.next()) {
				rdb.setId(rs.getInt("id"));
				rdb.setUserId(rs.getInt("user_id"));
				rdb.setStartDate(rs.getDate("start_date"));
				rdb.setEndDate(rs.getDate("end_date"));
				rdb.setMessage(rs.getString("message"));
				rdb.setReqestDate(rs.getDate("request_date"));
				rdb.setItemId(rs.getInt("item_id"));
			}

			System.out.println("searching rental by userID has been completed");
			return rdb;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//レンタル情報<リスト>取得[UserId]
	public static ArrayList<RentalDataBeans> UserIdDetails(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_rental"
										+ " JOIN t_item"
										+ " ON t_rental.item_id = t_item.id"
										+ " WHERE t_rental.user_id = ?");

			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			ArrayList<RentalDataBeans> RentalList = new ArrayList<RentalDataBeans>();

			while (rs.next()) {
				RentalDataBeans rdb = new RentalDataBeans();
				rdb.setId(rs.getInt("id"));
				rdb.setStartDate(rs.getDate("start_date"));
				rdb.setEndDate(rs.getDate("end_date"));
				rdb.setMessage(rs.getString("message"));
				rdb.setReqestDate(rs.getDate("request_date"));
				rdb.setItemId(rs.getInt("item_id"));
				rdb.setrInstrument(rs.getString("instrument"));
				rdb.setRentalFlag(rs.getInt("rentalFlag"));
				RentalList.add(rdb);
			}

			System.out.println("searching RentalDataBeans by userId has been completed");
			return RentalList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
