package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemCategoryDataBeans;

public class ItemCategoryDataDAO {

	public static ArrayList<ItemCategoryDataBeans> findAll() throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_itemCategoryData");
			ResultSet rs = st.executeQuery();
			ArrayList<ItemCategoryDataBeans> icdDList = new ArrayList<ItemCategoryDataBeans>();

			while (rs.next()) {
				ItemCategoryDataBeans icdd = new ItemCategoryDataBeans();
				icdd.setId(rs.getInt("id"));
				icdd.setCategoryId(rs.getInt("Category_id"));
				icdd.setCategoryDataName(rs.getString("CategoryData_name"));
				icdDList.add(icdd);
			}

			System.out.println("searching ItemCategoryDataBeans  has been completed");
			return icdDList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


}
