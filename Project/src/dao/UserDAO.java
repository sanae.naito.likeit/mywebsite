package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

public class UserDAO {
	//セッションから指定データを取得（削除も一緒に行う）
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

	//パスワード暗号化
	public static String magic(String password) {
		String source = password ;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		System.out.println(result);
		return result;
	}

	//Login検索
	public static int getUserId(String loginId,String password) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		String pass = magic(password);
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_user WHERE login_id = ?");
			st.setString(1,loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while(rs.next()) {
				if(pass.equals(rs.getString("login_password"))){
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}
			System.out.println("searching userId by loginId has been completed");
			return userId;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//新規登録
	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_user(name,login_id,login_password,introduction) VALUES(?,?,?,?)");
			st.setString(1,udb.getName());
			st.setString(2,udb.getLoginId());
			st.setString(3,magic(udb.getPassword()));
			st.setString(4, udb.getIntroduction());
			st.executeUpdate();
			System.out.println("inserting user has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//[logonId]重複チェック
	public static boolean findByloginId(String loginId,int userId) throws SQLException{
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT login_id FROM t_user WHERE login_id = ? AND id != ?");
			st.setString(1,loginId);
			st.setInt(2,userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	//User更新
	public static void Update(UserDataBeans udb) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			//Update
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET name = ?,introduction = ? WHERE id = ?;");
			st.setString(1, udb.getName());
			st.setString(2, udb.getIntroduction());
			st.setInt(3, udb.getId());
			st.executeUpdate();

			System.out.println("update has been completed");

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}

	//Mypage[User詳細]
	public static UserDataBeans Details(int userId) throws SQLException{
		UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con =DBManager.getConnection();
			st = con.prepareStatement("SELECT id,name,login_id,introduction FROM t_user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setIntroduction(rs.getString("introduction"));
			}
			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by userId has been completed");
		return udb;
	}
}
