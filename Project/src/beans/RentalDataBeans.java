package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RentalDataBeans implements Serializable{

	private int id;
	private int userId;
	private Date startDate;
	private Date endDate;
	private String message;
	private Date reqestDate;
	private int itemId;

	private int totalPrice;
	private String rInstrument;
	private int rentalFlag;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getReqestDate() {
		return reqestDate;
	}
	public void setReqestDate(Date reqestDate) {
		this.reqestDate = reqestDate;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getFormatTotalPrice(int totalPrice) {
		return String.format("%,d", totalPrice);
	}
	public String getFormatDate() {

		if(startDate== null) {
			return "";
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		return sdf.format(startDate);
	}
	public String getFormatDate2() {

		if(endDate== null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		return sdf.format(endDate);
	}
	public String getFormatReqestDate() {
		if(reqestDate== null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		return sdf.format(reqestDate);
	}
	public String getrInstrument() {
		return rInstrument;
	}
	public void setrInstrument(String instrument) {
		this.rInstrument = instrument;
	}
	public int getRentalFlag() {
		return rentalFlag;
	}
	public void setRentalFlag(int rentalFlag) {
		this.rentalFlag = rentalFlag;
	}
}
