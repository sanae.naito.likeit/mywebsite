package beans;

import java.io.Serializable;

public class ItemCategoryBeans implements Serializable{

	private int id;
	private String categoryName;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategoryName() {
		return this.categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
