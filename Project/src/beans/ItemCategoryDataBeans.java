package beans;

import java.io.Serializable;

public class ItemCategoryDataBeans implements Serializable{
	//Beansは小文字
	private int id;
	private int categoryId;
	private String categoryDataName;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryDataName() {
		return categoryDataName;
	}
	public void setCategoryDataName(String categoryDataName) {
		this.categoryDataName = categoryDataName;
	}

}
