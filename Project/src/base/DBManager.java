package base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
	    private static String url = "jdbc:mysql://localhost/";
	    private static String user = "root";
	    private static String pass = "password";
	    private static String db_name = "mc_db";
	    private static String PARAMETERS = "?useUnicode=true&characterEncoding=utf8";

	    public static Connection getConnection() {
	        Connection con = null;
	        try {
	            Class.forName("com.mysql.jdbc.Driver");
	            con = DriverManager.getConnection(url+db_name+PARAMETERS,user,pass);
	            System.out.println("DBConnected!!");
	        } catch (SQLException | ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	        return con;
	    }
	}
