<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<style>/*
	<!-- Materialize -->
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="Materialize/css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
*/</style>

    <title>itemList</title>
  </head>

     <!--ナビバー-->
      <nav class="navbar navbar-light" style="background-color: #FFDEAD;">
   <a class="navbar-brand h3" href="Home">Musicycling</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item active">
        <a class="nav-link" href="Home">Home<span class="sr-only">(current)</span></a>
      </li>

      <!-- ログイン〇→Logout / ログイン×→Login-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="Login">Login</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Logout">Logout</a>
	  </c:otherwise>
	  </c:choose>
      </li>

      <!-- ログイン〇→Mypage / ログイン×→MypageCreate-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="MypageCreate">Mypage Create</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Mypage">My page</a>
	  </c:otherwise>
	  </c:choose>
      </li>

	 <!-- ログイン〇→× / ログイン×→RentalExhibitCreate-->
     <li class="nav-item">
 	  <c:choose>
      <c:when test="${isLogin == false}">
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="RentalExhibitCreate">Rental Exhibit</a>
	  </c:otherwise>
	  </c:choose>
    </li>

    </ul>
  </div>
       </nav>

    <!--本体-->
    <body>
    <div class="row">
    <br>
    <br>
    </div>
        <form action="itemList" method="GET">
         <!--検索-->
        <div class="row">
        <input type="text" name="search" class="mx-auto d-block">
        </div>
        <div class="row">
        <input type="submit" value="Search" class="btn btn-outline-warning mx-auto d-block">
        </div>
    	</form>
        <br><br>
       <div class="container">
      <div class="card-deck">
        <div class="row">
        <c:forEach var="item" items="${itemList}" varStatus="status">
          <div class="card" style="width: 40rem;">
            <img src="img/${item.fileName}" class="card-img-top" alt="instrument">
            <div class="card-body">
              <h5 class="card-title">${item.instrument}</h5>
              <p class="card-text">
               <table class="table table-sm">
                  <thead>
                    <tr>
                      <th scope="col">メーカ</th>
                      <th scope="col">レンタル可能期間</th>
                      <th scope="col">レンタル金額(1日)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>${item.manufacturer}</td>
                      <td>${item.formatDate}~${item.formatDate2}</td>
                      <td>${item.formatPrice}円</td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <div class="card-footer">
              <small class="text-muted">
               Username :<a type="button" href="UserDetails?id=${item.userId}" class="btn btn-link h6">${item.userName}</a>
              <a href="Rental?id=${item.id}"><img src="img/star.svg" class="rounded float-right" alt="Rental" width="32" height="32" title="Rental"></a>
              </small>
            </div>
            </div>
		<c:if test="${(status.index + 1) % 2 == 0}">
			</div>
			</div>
			</div>
			<br><br>
			<div class="container">
			<div class="card-deck">
			<div class="row">
				</c:if>
				</c:forEach>
			</div>
			</div>
			</div>

			<br><br>

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">

  <!-- chevron_left -->
  <c:choose>
    <c:when test = "${pageNum == 1}">
    <li class="page-item disabled"><a class="page-link" href="itemList?search=${search}&page_num=${pageNum - 1}">Back</a></li>
    </c:when>
    <c:otherwise>
    <li class="page-item"><a class="page-link" href="itemList?search=${search}&page_num=${pageNum - 1}">Back</a></li>
    </c:otherwise>
  </c:choose>

  <!-- index -->
    <c:forEach begin = "${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end = "${(pageNum + 5) > 0 ? pageMax : pageNum + 5}" step = "1" varStatus="status">
      <li <c:if test = "${pageNum == status.index}"> class="page-item"</c:if>><a class="page-link" href="itemList?search=${search}&page_num=${status.index}">${status.index}</a></li>
    </c:forEach>

  <!-- chevron_right -->
    <c:choose>
   	<c:when test = "${pageNum == pageMax || pageMax == 0}">
    <li class="page-item disabled"><a class="page-link" href="itemList?search=${search}&page_num=${pageNum + 1}">Next</a></li>
    </c:when>
    <c:otherwise>
    <li class="page-item"><a class="page-link" href="itemList?search=${search}&page_num=${pageNum + 1}">Next</a></li>
    </c:otherwise>
   </c:choose>

  </ul>
</nav>

        <div class="row col-4 float-right">
        <a type="button" href="Home" class="btn btn-link mx-auto d-block">back</a>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<style>/*
	<!-- Materialize -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="Materialize/js/materialize.min.js"></script>
*/</style>

    </body>
</html>