<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Rental</title>
  </head>

      <!--ナビバー-->
      <nav class="navbar navbar-light" style="background-color: #FFDEAD;">
   <a class="navbar-brand h3" href="Home">Musicycling</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item active">
        <a class="nav-link" href="Home">Home<span class="sr-only">(current)</span></a>
      </li>

      <!-- ログイン〇→Logout / ログイン×→Login-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="Login">Login</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Logout">Logout</a>
	  </c:otherwise>
	  </c:choose>
      </li>

      <!-- ログイン〇→Mypage / ログイン×→MypageCreate-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="MypageCreate">Mypage Create</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Mypage">My page</a>
	  </c:otherwise>
	  </c:choose>
      </li>

	 <!-- ログイン〇→× / ログイン×→RentalExhibitCreate-->
     <li class="nav-item">
 	  <c:choose>
      <c:when test="${isLogin == false}">
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="RentalExhibitCreate">Rental Exhibit</a>
	  </c:otherwise>
	  </c:choose>
    </li>

    </ul>
  </div>
       </nav>

    <!--本体-->
    <body>
    <div class="row">
    <br>
    <br>
    </div>
    <div class="container">
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	</div>
		<form action= "Rental" method="POST">
        <h5 class="text-center">レンタル申請を行いますか？</h5>
        <div class="row col-md-2 mx-auto d-block">
        			<input type="submit" value="YES" class="btn btn-outline-danger float-left">
                    <a type="button" class="btn btn-outline-primary float-right" href="itemList?search=${search}">NO</a>
        </div>
        <br><br>
        <div class="container">
        <div class="col-md-6">
        <div class="card float-left">
            <img src="img/${item.fileName}" class="card-img-top" alt="instrument">
             <div class="card-body">
              <h5 class="card-title">${item.instrument}</h5>
              <p class="card-text">
            	<table class="table table-sm">
                  <thead>
                    <tr>
                      <th scope="col">メーカ</th>
                      <th scope="col">レンタル可能期間</th>
                      <th scope="col">レンタル金額(1日)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>${item.manufacturer}</td>
                      <td>${item.formatDate}~${item.formatDate2}</td>
                      <td>${item.formatPrice}円</td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <div class="card-footer">
              <small class="text-muted">
                  <h6> Username : ${item.userName} </h6>
              </small>
            </div>
          </div>
         </div>
            <div class="card border-warning" style="max-width: 200ren;">
              <div class="card-header"><b>入力フォーム</b>　＊出展者側に送信されます</div>
              <div class="card-body text-secondary">
                <p class="card-text">
            <div class="row mx-auto d-block">
            <input type="hidden" name="item_id" value="${item.id}">
                <b>レンタル期間</b>
                <div class="input-group-prepend">
                <div class="col-md-5">
                <input type="date" name ="start_date" class="form-control" required>
                </div>
                ～
                <div class="col-md-5">
                <input type="date" name ="end_date" class="form-control" required>
                </div>
                </div>
            </div>
                <br><br>
            <div class="row mx-auto d-block">
            <div class="col-sm-20" style="padding: 3px;">
              <textarea name="Messege" class="form-control input-sm text-left"  rows="3" id="comment" placeholder="Messege etc…"></textarea>
            </div>
            </div>
             </div>
            </div>
        </div>
        </form>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>