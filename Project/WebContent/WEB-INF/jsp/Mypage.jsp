<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Mypage</title>
  </head>

     <!--ナビバー-->
      <nav class="navbar navbar-light" style="background-color: #FFDEAD;">
   <a class="navbar-brand h3" href="Home">Musicycling</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item active">
        <a class="nav-link" href="Home">Home<span class="sr-only">(current)</span></a>
      </li>

      <!-- ログイン〇→Logout / ログイン×→Login-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="Login">Login</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Logout">Logout</a>
	  </c:otherwise>
	  </c:choose>
      </li>

	 <!-- ログイン〇→× / ログイン×→RentalExhibitCreate-->
     <li class="nav-item">
 	  <c:choose>
      <c:when test="${isLogin == false}">
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="RentalExhibitCreate">Rental Exhibit</a>
	  </c:otherwise>
	  </c:choose>
    </li>

    </ul>
  </div>
       </nav>

     <!--本体-->
    <body>
    <div class="row">
    <br>
    <br>
    </div>
        <h1 class="text-center text-warning">My page</h1>
        <br><br>
        <!--Your Data-->
        <form action="Mypage" method="POST">
        <div class="card border-success mb-3 mx-auto" style="max-width: 50rem;">
          <div class="card-header text-success h4">Your Data</div>
		<input type="hidden" name="id" value="${udb.id}">
          <div class="card-body">
          <div class="row col-md-5 mx-auto d-block">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-default">Username</span>
                <input type="text" name="name" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="${udb.name}">
              </div>
           </div>
              <br>
             <div class="row mx-auto d-block">
            <div class="col-sm-20" style="padding: 3px;">
              <textarea name = "introduction" class="form-control text-left input-sm" rows="3" id="comment" placeholder="introduction etc…">
              ${udb.introduction}</textarea>
            </div>
            </div>
            <br><br>
            <div class="row col-md-2 mx-auto d-block">
            	<input type="submit" value="Update" class="btn btn-outline-success">
            </div>
         </div>
         </div>
        </form>
        <br><br>
        <!--Rental History-->
        <div class="card border-warning mb-3 mx-auto" style="max-width: 50rem;">
          <div class="card-header text-warning h4">Rental History</div>
          <div class="card-body">
            <table class="table table-sm">
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col"></th>
                      <th scope="col"></th>
                      <th scope="col">instrument</th>
                    </tr>
                  </thead>
                  <tbody>
               <c:forEach var="Rental" items="${RentalList}" >
                <tr>
                  <th scope="row"><a href="RentalDetails?id=${Rental.id}"><img src="img/music1.svg" alt="Rental Details" width="32" height="32" title="Rental Details"></a></th>
                  		<c:choose>

                          <c:when test = "${Rental.rentalFlag == 1}">
                          <td>
                          <div class="text alert alert-warning col-7" role="alert">
                          <b>${Rental.formatDate} 借入日</b>
                          </div>
                          </td>
                          <td>
                          <div class="text alert alert-danger col-7" role="alert">
                          <b>${Rental.formatDate2} 返却日</b>
                          </div>
                          </td>
                          </c:when>

                          <c:when test = "${Rental.rentalFlag == 2}">
                          <td></td>
                          <td><div class="text-center alert alert-dark col-7" role="alert">
                          <b>終了</b>
                          </div></td>
                          </c:when>

                        </c:choose>
                  <td>${Rental.rInstrument}</td>
                </tr>
               </c:forEach>
                  </tbody>
                </table>
           </div>
         </div>
         <br><br>
      <!--Rental Exhibit History-->
      <div class="card border-dark mb-3 mx-auto" style="max-width: 50rem;">
          <div class="card-header text-dark h4">Rental Exhibit History</div>
          <div class="card-body">
            <table class="table table-sm">
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th scope="col"></th>
                  <th scope="col">instrument</th>
                </tr>
              </thead>
              <tbody>

              <c:forEach var="item" items="${itemList}" >
                <tr>
                  <th scope="row"><a href="RentalExhibitDetails?id=${item.id}"><img src="img/music2.svg" alt="Rental Exhibit" width="32" height="32" title="Rental Exhibit"></a></th>
                  		<c:choose>

                          <c:when test = "${item.rentalFlag == 0}">
                          <td><div class="text-center alert alert-success col-7" role="alert">
                          <b>貸し出し可能
                          <a type="button" class="btn btn-outline-success" href="RentalFlag?id=${item.id}">終了</a>
                          </b>
                          </div></td>
                          </c:when>

                          <c:when test = "${item.rentalFlag == 1}">
                          <td><div class="text-center alert alert-danger col-7" role="alert">
                          <b>貸し出し中
                          <!-- 返却時のボタン付ける  -->
                          <a type="button" class="btn btn-outline-danger" href="RentalFlag?id=${item.id}">RETURN</a>
                          </b>
                          </div></td>
                          </c:when>

                          <c:when test = "${item.rentalFlag == 2}">
                          <td><div class="text-center alert alert-dark col-7" role="alert">
                          <b>終了</b>
                          </div></td>
                          </c:when>

                        </c:choose>
                  <td>${item.instrument}</td>
                </tr>
               </c:forEach>

              </tbody>
            </table>
           </div>
       </div>
       <br><br>

    <div class="row col-4 float-right">
      <a type="button" href="Home" class="btn btn-link mx-auto d-block">back</a>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>