<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Home</title>
        </head>

     <!--ナビバー-->
      <nav class="navbar navbar-light" style="background-color: #FFDEAD;">
   <a class="navbar-brand h3" href="Home">Musicycling</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item active">
        <a class="nav-link" href="Home">Home<span class="sr-only">(current)</span></a>
      </li>

      <!-- ログイン〇→Logout / ログイン×→Login-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="Login">Login</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Logout">Logout</a>
	  </c:otherwise>
	  </c:choose>
      </li>

      <!-- ログイン〇→Mypage / ログイン×→MypageCreate-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="MypageCreate">Mypage Create</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Mypage">My page</a>
	  </c:otherwise>
	  </c:choose>
      </li>

	 <!-- ログイン〇→× / ログイン×→RentalExhibitCreate-->
     <li class="nav-item">
 	  <c:choose>
      <c:when test="${isLogin == false}">
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="RentalExhibitCreate">Rental Exhibit</a>
	  </c:otherwise>
	  </c:choose>
    </li>

    </ul>
  </div>
       </nav>

    <!--本体-->
    <body>
    <div class="row">
    <br>
    <br>
    </div>
        <h1 class="text-center text-warning">Musicycling</h1>
    <div class="row">
    <br><br>
    </div>
    <form action="itemList" method="GET">
         <!--検索-->
        <div class="row">
        <input type="text" name="search" class="mx-auto d-block">
        </div>
        <div class="row">
        <input type="submit" value="Search" class="btn btn-outline-warning mx-auto d-block">
        </div>
     </form>
    <div class="row">
    <br><br>
    </div>

<div class="row col-4 mx-auto">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active  center-block">
      <img src="img/saxophone.jfif" alt="saxophone" wide="400" height="400">
    </div>
    <div class="carousel-item  center-block">
      <img src="img/guitar.jpg"  alt="guitar" wide="400" height="400">
    </div>
    <div class="carousel-item  center-block">
      <img src="img/flute.jpg" alt="flute" wide="400" height="400">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
    </div>
</div>
    <div class="row">
    <br><br>
    </div>

     <!--カテゴリーリンク-->
<div class="row col-4 mx-auto">
    <div class="dropdown">
        <button class="btn btn-outline-dark dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        弦楽器
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
        <c:forEach var="category" items="${CategoryList}">
        <c:if test="${category.categoryId == 1}">
        <a type="button" class="dropdown-item" href="categoryList?id=${category.id}">${category.categoryDataName}</a>
        </c:if>
   		</c:forEach>
        </div>
    </div>

    <div class="dropdown">
         <button class="btn btn-outline-dark dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        管楽器
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu3">
        <c:forEach var="category" items="${CategoryList}">
        <c:if test="${category.categoryId == 2}">
        <a type="button" class="dropdown-item" href="categoryList?id=${category.id}">${category.categoryDataName}</a>
        </c:if>
   		</c:forEach>
        </div>
    </div>

    <div class="dropdown">
        <button class="btn btn-outline-dark dropdown-toggle" type="button" id="dropdownMenu4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        鍵盤楽器
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu4">
        <c:forEach var="category" items="${CategoryList}">
        <c:if test="${category.categoryId == 3}">
        <a type="button" class="dropdown-item" href="categoryList?id=${category.id}">${category.categoryDataName}</a>
        </c:if>
        </c:forEach>
        </div>
    </div>
    	<c:forEach var="category" items="${CategoryList}">
    	<c:if test="${category.categoryId == 4}">
    	<a type="button" class="btn btn-outline-dark" href="categoryList?id=${category.id}">${category.categoryDataName}</a>
    	</c:if>
    	</c:forEach>
    	<c:forEach var="category" items="${CategoryList}">
    	<c:if test="${category.categoryId == 5}">
    	<a type="button" class="btn btn-outline-dark" href="categoryList?id=${category.id}">${category.categoryDataName}</a>
    	</c:if>
       	</c:forEach>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>