<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>RentalResult</title>
  </head>

     <!--ナビバー-->
      <nav class="navbar navbar-light" style="background-color: #FFDEAD;">
   <a class="navbar-brand h3" href="Home">Musicycling</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item active">
        <a class="nav-link" href="Home">Home<span class="sr-only">(current)</span></a>
      </li>

      <!-- ログイン〇→Logout / ログイン×→Login-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="Login">Login</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Logout">Logout</a>
	  </c:otherwise>
	  </c:choose>
      </li>

      <!-- ログイン〇→Mypage / ログイン×→MypageCreate-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="MypageCreate">Mypage Create</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Mypage">My page</a>
	  </c:otherwise>
	  </c:choose>
      </li>

	 <!-- ログイン〇→× / ログイン×→RentalExhibitCreate-->
     <li class="nav-item">
 	  <c:choose>
      <c:when test="${isLogin == false}">
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="RentalExhibitCreate">Rental Exhibit</a>
	  </c:otherwise>
	  </c:choose>
    </li>

    </ul>
  </div>
       </nav>

   <!--本体-->
    <body>
    <div class="row">
    <br>
    <br>
    </div>
        <h1 class="text-center text-warning">Musicycling</h1><br>
        <h5 class="text-center">申請が終了しました。</h5>
        <div class="row col-md-2 mx-auto d-block">
				    <a type="button" class="btn btn-outline-info float-left" href="Home">HOME</a>
                    <a type="button" class="btn btn-outline-success float-right" href="Mypage">Mypage</a>
        </div>
        <br><br><br>
       <div class="card border-warning mb-10 mx-auto" style="max-width: 50rem;">
          <div class="card-header text-dark h4">Reqest Details</div>
          <div class="card-body">
            <table class="table table-sm">
              <thead>
                <tr>
                  <th scope="col">Reqest Day</th>
                  <th scope="col">Reqest Username</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>${RentalInfo.formatReqestDate}</td>
                  <td>${item.userName}</td>
                </tr>
              </tbody>
            </table>
              <table class="table table-sm">
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th scope="col">Days</th>
                  <th scope="col">instrument</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row"><a href="RentalDetails?id=${RentalInfo.id}"><img src="img/star-b.svg" alt="RentalDetails" width="32" height="32" title="RentalDetails"></a></th>
                  <td>${RentalInfo.formatDate}~${RentalInfo.formatDate2}</td>
                  <td>${item.instrument}</td>
                </tr>
              </tbody>
            </table>
           </div>
       </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>