<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>MypageCreate</title>
  </head>

    <!--ナビバー-->
      <nav class="navbar navbar-light" style="background-color: #FFDEAD;">
   <a class="navbar-brand h3" href="Home">Musicycling</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item active">
        <a class="nav-link" href="Home">Home<span class="sr-only">(current)</span></a>
      </li>

      <!-- ログイン〇→Logout / ログイン×→Login-->
      <li class="nav-item">
      <c:choose>
      <c:when test="${isLogin == false}">
      	<a class="nav-link" href="Login">Login</a>
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Logout">Logout</a>
	  </c:otherwise>
	  </c:choose>
      </li>

	<li class="nav-item">
 	  <c:choose>
      <c:when test="${isLogin == false}">
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="Rental">Rental</a>
	  </c:otherwise>
	  </c:choose>
    </li>

     <li class="nav-item">
 	  <c:choose>
      <c:when test="${isLogin == false}">
	  </c:when>
	  <c:otherwise>
		<a class="nav-link" href="RentalExhibitCreate">Rental Exhibit</a>
	  </c:otherwise>
	  </c:choose>
    </li>

    </ul>
  </div>
       </nav>

     <!--本体-->
    <body>
    <div class="row">
    <br>
    <br>
    </div>
    <div class="container">
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	</div>

        <div class="card border-success mb-3 mx-auto" style="max-width: 40rem;">
            <div class="h3 card-header text-success">　　　　　　　　SIGHN UP</div>
        <div class="card-body text-success">
<form action= "MypageCreate" method="POST">
<div class="form-row">
    <div class="col-6 mb-3">
      <label>LoginId</label>
      <input type="text" name="login_id" class="form-control" placeholder="LoginId" required>
    </div>
    <div class="col-6 mb-3">
      <label>Username</label>
      <input type="text" name="name" class="form-control" placeholder="Username" required>
    </div>
</div>
  <div class="form-row">
    <div class="col-6 mb-3">
      <label>Password</label>
      <input type="password" name="password" class="form-control" placeholder="Password" required>
    </div>
    <div class="col-6 mb-3">
      <label>Password Verification</label>
      <input type="password" name="password2" class="form-control" placeholder="Password Verification" required>
    </div>
  </div>
      <div class="row mx-auto d-block">
            <div class="col-sm-20" style="padding: 3px;">
              <textarea name="introduction" class="form-control input-sm text-left"  rows="3" id="comment" placeholder="introduction etc…"></textarea>
            </div>
      </div>
      <br>
  <div class="form-row">
    <div class="form-group ">
    <div class="form-check">
      <input class="form-check-input is-invalid" type="checkbox" value="" id="invalidCheck3" required>
      <label class="form-check-label" for="invalidCheck3">
        Agree to terms and conditions
      </label>
    </div>
    </div>
  </div>
  <div class="form-row">
  <input type="submit" value="Submit form" class="btn btn-primary mx-auto">
  </div>
  </form>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>